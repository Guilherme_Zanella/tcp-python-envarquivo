import socket, os
import threading
from tkinter import filedialog
from tkinter import Tk

class Mensagem(object):
    def __init__(self):
        self.User = ''
        self.Message     = ''

root = Tk()
root.withdraw()

def rodaThread(conn):
    # buffer size
    CHUNK_SIZE = 8 * 1024

    while True:
        # message return
        data = conn.recv(CHUNK_SIZE)

        #operation the user
        # f - archive
        # q - exit
        if data.decode() == 'f':

            # message with the file name
            data = conn.recv(CHUNK_SIZE)
            print(data.decode())

            # creates the file, and loop
            with open('new_'+data.decode(), 'wb') as f:
                # receives file in bytes
                chunk = conn.recv(CHUNK_SIZE)
                # writes the received bytes to the file
                f.write(chunk)

            print("Received")

            #data = conn.recv(CHUNK_SIZE)
            #mySocket.sendfile(data, 0)
            #mySocket.send(data)

        elif data.decode() == 'q':
            conn.close()
            break
        else:
            # Print message 
            print('>>> {}'.format(data.decode()))



def Main():
    host = '127.0.0.1'
    port = 10000

    # Creates the client socket
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Makes the connection to the server
    mySocket.connect((host,port))

    t = threading.Thread(target=rodaThread, args=(mySocket,))
    t.start()

    # Client name
    objEnviar = Mensagem()
    objEnviar.User = input("Name: ")

    while True:

        message = input("Enter your message -> (f to file)(q Exit) ")

        if (message == 'f'):

            # Sends the message notifying the server
            mySocket.send(message.encode())

            # Opens a screen for choosing the file
            file_path = filedialog.askopenfilename(initialdir="/", title="Escolha um arquivo", filetypes=(("jpeg files", "*.jpg"), ("all files", "*.*")))
            print(file_path)

            # Send message with the name of the file
            mySocket.send( os.path.basename( file_path ).encode() )

            # Opens the chosen file
            with open(file_path, 'rb') as f:
                mySocket.sendfile(f, 0)

            # Sends the completed message
            print("file sent")

        elif (message == 'q'):
            mySocket.send(message.encode())
            mySocket.close()
            break
        else:
            mySocket.send(message.encode())


if __name__ == '__main__':
    Main()