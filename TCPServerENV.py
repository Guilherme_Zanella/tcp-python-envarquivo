import socket
import threading
import pickle
Clients = []

class Mensagem(object):
    def __init__(self):
        self.User = ''
        self.Message     = ''
        
def rodaThread(conn):
    while True:
        
        print('Waiting for messages')
        data = conn.recv(8192)
        # Shows the message delivered
        print(data)
        # Deserializes the received message

        if not data:
            break


        for i in Clients:
            if conn != i:
            # Returns the message to the client
                i.send(data)

    conn.close()
    return

def Main():
    host = "0.0.0.0"
    port = 10000
    socketTCP = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socketTCP.bind((host,port))
    socketTCP.listen(1)
    print('Server TCP: {}:{}'.format(host,port))

    while True:
        # Blocked waiting for a client to connect
        conn, addr = socketTCP.accept()
        print ("Connection made by: " + str(addr))

        Clients.append(conn)
        
        # Creates and triggers the execution of the client's thred
        t = threading.Thread(target=rodaThread, args=(conn,))
        t.start()

    socket.close()

if __name__ == '__main__':
    Main()
